# Node task

This project contains a node hello-world

## Installation

1. Copy the repository
2. Install dependencies
3. Start the app


Code block of parameters and returns

```sh
const add = (a,b) => {
    return a + b;
}

app.get('/', (req, res) => {
    // const a = pars
    const x  = add(1, 2);
    res.send(`Summa: ${x}`);
})
```

![alternative text for test image](https://i.imgur.com/MAiUZhI.gif)
const express = require('express')
const app = express()
const port = 3000

/**
 * 
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a,b) => {
    return a + b;
}

app.get('/', (req, res) => {
    // const a = pars
    const x  = add(1, 2);
    res.send(`Summa: ${x}`);
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})